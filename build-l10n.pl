#!/usr/bin/perl

use Getopt::Long;
use Smart::Comments '###';
use Time::Piece;
use Date::Parse;

my $branch;
my $tag;
GetOptions(
    "branch=s" => \$branch,
    "tag=s"    => \$tag
) or die ("branch or tag args are missing\n");

# example ./build-l10n.pl -b 23.11 -t v23.11.01

my $rc;
my $inc = 1;
### $branch
### $tag

# v23.11 -> 23.11-1
my $tag2 = $tag;
$tag2 =~ s/^v//;
$tag2 =~ s/-1$//;
### $tag
#### $tag2

qx(cd /home/mason/git/l10n/koha-l10n; 
 git clean -fd ;
 git fetch ;
 git branch $branch ;
 git checkout $branch ;
);

$rc = qx(cd /home/mason/git/l10n/koha-l10n;
 git reset --hard origin/$branch );
die "remote branch $branch doesnt exist" unless $rc;

### $rc;
#exit;

# get tag date

my $tag_commit =
qx(cd /home/mason/pb/pb-master/koha ;  git show --date=iso $tag  -1  --pretty=format:"%h" -s );
my $tag_date =
qx(cd /home/mason/pb/pb-master/koha ;  git show --date=iso $tag  -1  --pretty=format:"%ad" -s );

chomp $tag_date;
chomp $tag_commit;

### $tag_date
### $tag_commit

# get lang commit before tag date

#$DB::single = 1;

# set branch
qx(cd /home/mason/git/l10n/koha-l10n  ;  git fetch origin);
qx(cd /home/mason/git/l10n/koha-l10n  ;  git checkout $branch);

# %h short commit hash
# %ad author data

my $lang_commit =
qx(cd /home/mason/git/l10n/koha-l10n  ;  git log --before="$tag_date" --pretty=format:"%h" -s -1 );

my $lang_date =
qx(cd /home/mason/git/l10n/koha-l10n  ;  git show --date=iso $lang_commit -1  --pretty=format:"%ad" -s );

chomp $lang_commit;
chomp $lang_date;

### $lang_commit
### $lang_date

die 'no matching l10n.git commit found' unless $lang_commit;
die 'no matching koha.git tag found'    unless $tag_commit;

my $time1 = str2time($tag_date);
my $time2 = str2time($lang_date);

my $t1 = localtime($time1)->strftime('%F %T');
my $t2 = localtime($time2)->strftime('%F %T');

### $t1
### $t2

# sync branch
qx(cd /home/mason/git/l10n/koha-l10n ;
    git reset --hard ;
    git branch -D $tag ;
    git branch $tag ;
    git checkout $tag )
;

#qx(cd /home/mason/git/l10n/koha-l10n ; git reset --hard origin/$branch);
qx(cd /home/mason/git/l10n/koha-l10n ; git reset --hard $lang_commit );

qx(cd /home/mason/git/l10n/koha-l10n ; DEBEMAIL='mtj\@kohaaloha.com'  DEBFULLNAME='Mason James'  dch --force-bad-version --distribution $branch --force-distribution -v "$tag2-$inc" 'Update version for $tag2 release');

# ---------------------------------------
# set commit
qx(cd /home/mason/git/l10n/koha-l10n ; git commit -m "Update debian/changelog file"  debian/changelog);
qx(cd /home/mason/git/l10n/koha-l10n ; dpkg-buildpackage -us -uc -b );

# ---------------------------------------
#qx|reprepro  --ignore=uploaders   -v -C main  -b  /home/mason/apt/kc/koha     remove  $branch  koha-l10n |;
#qx|reprepro  --ignore=uploaders   -v -C main  -b  /home/mason/apt/kc/koha      includedeb   $branch  /home/mason/git/l10n/koha-l10n_$tag2-$inc\_all.deb|;

